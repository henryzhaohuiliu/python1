'''
Created on Aug 10, 2017

@author: hliu
'''
from azure.common.credentials import ServicePrincipalCredentials
from azure.mgmt.network import NetworkManagementClient
from azure.mgmt.compute import ComputeManagementClient
from azure.common.client_factory import get_client_from_cli_profile

'''
prepare:
az login
az network vnet create -g sampleVmResourceGroup -n azure-sample-vnet --address-prefix 10.0.0.0/16 --subnet-name azure-sample-subnet --subnet-prefix 10.0.0.0/24
az network public-ip create -g sampleVmResourceGroup -n azure-sample-ip --allocation-method Dynamic --version IPv6
az network nic create -g sampleVmResourceGroup --vnet-name azure-sample-vnet --subnet azure-sample-subnet -n azure-sample-nic --public-ip-addressazure-sample-ip
az ad sp create-for-rbac --name "MY-PRINCIPAL-NAME" --password "STRONG-SECRET-PASSWORD"
'''

# Tenant ID for your Azure Subscription
TENANT_ID = '81ebe264-dfc5-4494-b3c5-c67721e5916d'

# Your Service Principal App ID
CLIENT = '52d8892a-4f9c-4e91-8fc3-be9ca156eb20'

# Your Service Principal Password
KEY = 'STRONG-SECRET-PASSWORD'

subscription_id = 'fbe84411-6771-4c44-aa42-585e8c6208f6'

credentials = ServicePrincipalCredentials(
    client_id = CLIENT,
    secret = KEY,
    tenant = TENANT_ID
)

print (credentials)

#network_client = get_client_from_cli_profile(NetworkManagementClient)
network_client = NetworkManagementClient(credentials, subscription_id)

# Azure Datacenter
LOCATION = 'eastus'

# Resource Group
GROUP_NAME = 'sampleVmResourceGroup'

# Network
VNET_NAME = 'azure-sample-vnet'
SUBNET_NAME = 'azure-sample-subnet'
# VM
NIC_NAME = 'azure-sample-nic'
VM_NAME = 'testLinuxVM'
# get nic id
nic = network_client.network_interfaces.get(GROUP_NAME, NIC_NAME)

print (nic)

